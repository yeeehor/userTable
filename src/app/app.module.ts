import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { UserListComponent } from './user-list/user-list.component';
import {ConnectionService} from './service/connection.service';
import {HttpClientModule} from '@angular/common/http';
import {MyDialogComponent} from './dialog/dialog.component';
import {BootstrapModalModule} from 'ng2-bootstrap-modal';
import {FormsModule} from '@angular/forms';
import { DialogEditComponent } from './dialog-edit/dialog-edit.component';


@NgModule({
  declarations: [
    AppComponent,
    UserListComponent,
    MyDialogComponent,
    DialogEditComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    BootstrapModalModule.forRoot({container: document.body})
  ],
  entryComponents: [MyDialogComponent, DialogEditComponent],
  providers: [ConnectionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
