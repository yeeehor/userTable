import { Component, OnInit } from '@angular/core';
import {User} from '../model/User';
import {DialogComponent, DialogService} from 'ng2-bootstrap-modal';
import {NgForm} from '@angular/forms';
import {ConfirmModel} from '../dialog/dialog.component';

export interface ConfirmModel {
  user: User;
}

@Component({
  selector: 'app-dialog-edit',
  templateUrl: './dialog-edit.component.html',
  styleUrls: ['./dialog-edit.component.css']
})
export class DialogEditComponent extends DialogComponent<ConfirmModel, User> {
  user: User;

  constructor(dialogService: DialogService) {
    super(dialogService);
  }

  confirm(forma: NgForm) {
    this.result = forma.value;
    this.close();
  }
}
