import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {ConnectionService} from '../service/connection.service';
import {User} from '../model/User';
import {MyDialogComponent} from '../dialog/dialog.component';
import {DialogService} from 'ng2-bootstrap-modal';
import {DialogEditComponent} from '../dialog-edit/dialog-edit.component';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  users: User[] = [];

  constructor(private userService: ConnectionService, private dialogService: DialogService) {
  }

  ngOnInit() {
    this.userService.getUsers().subscribe((resp: User[]) => {
      this.users.push(...resp);
    });
  }

  addUser() {
    let disposable = this.dialogService.addDialog(MyDialogComponent, {
      user: new User(),
    }).subscribe((user) => {
      if (user) {
      this.users.push(user);
      }
    });
  }

  editUser(currUser, index: number) {
    let disposable = this.dialogService.addDialog(DialogEditComponent, {
      user: Object.assign({}, currUser),
    }).subscribe((user) => {
      console.log(user);
      if (user) {
       this.users.splice(index, 1, user);
        this.users = this.users.slice();
      }
    });
  }
  delUser(index) {
    this.users.splice(index, 1);
    this.users = this.users.slice();
  }
}

