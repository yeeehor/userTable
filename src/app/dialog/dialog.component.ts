import {Component} from '@angular/core';
import {DialogComponent, DialogService} from 'ng2-bootstrap-modal';
import {User} from '../model/User';
import {NgForm} from '@angular/forms';

export interface ConfirmModel {
  user: User;
}

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class MyDialogComponent extends DialogComponent<ConfirmModel, User> {
  user: User;

  constructor(dialogService: DialogService) {
    super(dialogService);
  }

  confirm(forma: NgForm) {
    this.result = forma.value;
    this.close();
  }
}
