export class User {
  id: number;
  name: string;
  username: string;
  email: string;
  street: string;
  number: number;
}

